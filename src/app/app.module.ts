import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { MenuComponent } from './component/menu/menu.component';
import { ContactosComponent } from './component/contactos/contactos.component';
import { Opcion1Component } from './component/opcion1/opcion1.component';
import { Opcion2Component } from './component/opcion2/opcion2.component';
import { Opcion3Component } from './component/opcion3/opcion3.component';
import { Opcion4Component } from './component/opcion4/opcion4.component';
import { Opcion5Component } from './component/opcion5/opcion5.component';
import { APP_ROUTING } from './app.routers';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    MenuComponent,
    ContactosComponent,
    Opcion1Component,
    Opcion2Component,
    Opcion3Component,
    Opcion4Component,
    Opcion5Component,
    
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
